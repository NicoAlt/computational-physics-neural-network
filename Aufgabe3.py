import numpy as np
import matplotlib.pyplot as plt
from matplotlib import gridspec
from neuronen_verlagerung.neuronen_rechnung import runge_kutta_impulse

"""Zeit Intervall"""

t_ende = 50
t_start = 0

"""Berechnete Zeiten Impuls t2 von [0] I = 10 bis [9] I = 100"""

np.array([0, 24.3, 20.5, 18.9, 18, 17.3, 16.9, 16.5, 16.2, 16.0])


def plot_2_impulse(N, startvalues, I_imp, t1, t2):

    """ Der Aufbau der ganzen Funktion ist wie in der Aufgabe 2d).
        Hier verwenden wir die Runge-Kutta Methode mit zwei Impulsen.
        Deswegen übergeben wir die Zeitpunkte t1 und t2 neben der Anzahl der Stützstellen, den Anfangsbedingungen und der Stärke des Stromimpulses.
        Ansonsten sind die Plots und Listen die gleichen wie in der Aufgabe 2d)."""

    h = (t_ende - t_start) / N
    timestep = np.arange(t_start, t_ende, h)
    voltage_points, n_points, m_points, h_points = runge_kutta_impulse(N, startvalues, -5, t1, t2, I_imp)
    current1 = np.copy(timestep)
    current2 = np.copy(timestep)
    current1[current1 < t1] = 0
    current1[current1 > t1 + 1] = 0
    current1[current1 != 0] = I_imp
    current2[current2 < t2] = 0
    current2[current2 > t2 + 1] = 0
    current2[current2 != 0] = I_imp
    current = current1 + current2
    gs = gridspec.GridSpec(1, 2, width_ratios=[2, 2])
    plt.subplot(gs[0])
    plt.plot(timestep, voltage_points, label=r"U Runge_Kutta")
    plt.plot(timestep, current, label="Electric Current")
    plt.xlabel("Time [ms]")
    plt.ylabel("Voltage [mV] / Electric Current [nA]")
    plt.legend()
    plt.subplot(gs[1])
    plt.plot(timestep, n_points, label="n")
    plt.plot(timestep, m_points, label="m")
    plt.plot(timestep, h_points, label="h")
    plt.xlabel("Time [ms]")
    plt.ylabel("Gatingvariables")
    plt.legend()
    plt.show()


plot_2_impulse(10000, np.array([-65, 0.5, 0.01, 0.99]), 70, 10, 16.9)


def compute_t2(N, startvalues, t1, I_imp):

    """ Die Parameter sind wieder dieselben wie in der Funktion davor nur ohne der Zeit t2.
        Wir definieren uns wieder den Abstand der Stützstellen und das Zeitarray mit dem Abstand der Stützstellen.
        Die Zeit t2 soll größer als t1 sein, deshalb definieren wir uns eine Länge von 50ms-t1.
        Die Länge multiplizieren wir dann mit 10, damit wir genauere Werte für unsere Zeiten bekommen.
        Damit wir wieder bei unserer echten Zeit sind, addieren wir auf den Zeitpunkt t wieder die schon verbrauchte Zeit t1 drauf.
        Mit diesem t lösen wir dann die DGLS mithilfe der Runge-Kutta Methode für mehr Impulse.
        Dabei rechnen wir t/10 damit wir eine Zeit mit Nachkommastelle erhalten.
        Wenn ein Element nach t1+4ms eine Spannung größer als 0 besitzt, also ein Aktionspotential, dann soll der Zeitpunkt als return gegeben werden.
        Wir wählen t1+4ms, da nach ungefähr 4 Millisekunden nach dem Impuls das Aktionspotential vorbei ist."""

    h = (t_ende - t_start) / N
    timestep = np.arange(t_start, t_ende, h)
    t2 = t_ende - t1
    for t in range(t2 * 10):
        t = t + t1
        U_list = runge_kutta_impulse(N, startvalues, -5, t1, t / 10, I_imp)[0]
        for index in range(len(U_list)):
            if U_list[index] > 0 and timestep[index] > 14:
                return t / 10