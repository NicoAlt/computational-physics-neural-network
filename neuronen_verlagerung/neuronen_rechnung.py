from scipy.integrate import odeint
import numpy as np

"""Elektrische Konstanten"""

V_pot = -65
C = 1
U_K = -77
U_Na = 50
U_L = -54.387

"""g Dach Konstanten"""

g_K = 36
g_Na = 120
g_L = 0.3

"""Zeit Intervall in ms"""

t_start = 0
t_ende = 50

"""Hier sind alle DGLs aufgeführt, die für die Aufgaben benötigt werden"""


def alpha_n(U):
    return -0.01 * (55 + U) / (np.exp(-(55 + U) / 10) - 1)


def alpha_m(U):
    return -0.1 * (40 + U) / (np.exp(-(40 + U) / 10) - 1)


def alpha_h(U):
    return 0.07 * np.exp(-(65 + U) / 20)


def beta_n(U):
    return 0.125 * np.exp(-(65 + U) / 80)


def beta_m(U):
    return 4 * np.exp(-(65 + U) / 18)


def beta_h(U):
    return 1 / (np.exp(-(35 + U) / 10) + 1)


def I_K(U, n):
    return g_K * n ** 4 * (U - U_K)


def I_Na(U, m, h):
    return g_Na * m ** 3 * h * (U - U_Na)


def I_L(U):
    return g_L * (U - U_L)


def DGL_U(I, U, n, m, h):
    return (I - I_K(U, n) - I_Na(U, m, h) - I_L(U)) / C


# Funktionen für Aufgabe 2 a-c)

def hilfsfunktion(startvalues, t, current):

    """ Hier werden die Steigungen der Spannung und der Gatingvariablen berechnet.
        Die Parameter sind dabei die Anfangsbedingungen, sowie der konstante Strom.
        Die Zeit wird hier nur als Formale mitgegeben, hat aber keinen Einfluss auf die Rechnungen.
        Der return ist dabei die  Steigung der Spannung und der Gatingvariablen."""

    U_given = startvalues[0]
    n = startvalues[1]
    m = startvalues[2]
    h = startvalues[3]
    hf1 = DGL_U(current, U_given, n, m, h)
    hf2 = alpha_n(U_given) * (1 - n) - beta_n(U_given) * n
    hf3 = alpha_m(U_given) * (1 - m) - beta_m(U_given) * m
    hf4 = alpha_h(U_given) * (1 - h) - beta_h(U_given) * h
    return np.array([hf1, hf2, hf3, hf4], float)


# Aufgabe 2a) wird in Aufgabe2.py verwendet


def expliciteuler(N, startvalues, current):  # Aufgabe 2a)

    """ Das hier ist die Aufgabe 2a). Die Vorlage für die Methode stammt aus der Vorlesung.
        Dabei ist h der Abstand der Stützstellen und Timesteps die Zusammensetzung der Arrays, aus dem Intervall [0,50] und der Abstand der Stützstellen h.
        Als Parameter werden hier die Anzahl der Stützstellen, die Anfangsbedingungen und der Konstante Strom übergeben.
        Die Hilfsfunktion wird dabei verwendet um die Aktuellen Variablen zu erhalten.
        Am Ende wird der Spannungsverlauf für ein inaktives Neuron ausgegeben."""

    h = (t_ende - t_start) / N
    timesteps = np.arange(t_start, t_ende, h)
    U = []
    for t in timesteps:
        U.append(startvalues[0])
        startvalues += h * hilfsfunktion(startvalues, t, current)
    return U


def runge_kutta(N, startvalues, current):

    """ Die hier verwendete Runge-Kutta Methode ist die gleiche wie in der Vorlesung.
        Als Paramter werden hier die gleichen wie in der expliziten Euler verwendet.
        Hier wird die Hilfsfunktion von vorher verwendet um immer wieder die angepassten Variablen zu bekommen.
        Als return erhalten wir hier den Spannungsverlauf eines Neuron, welches inaktiv ist."""

    h = (t_ende - t_start) / N
    timesteps = np.arange(t_start, t_ende, h)
    U = []
    for t in timesteps:
        U.append(startvalues[0])
        k1 = h * hilfsfunktion(startvalues, t, current)
        k2 = h * hilfsfunktion(startvalues + 0.5 * k1, t + 0.5 * h, current)
        k3 = h * hilfsfunktion(startvalues + 0.5 * k2, t + 0.5 * h, current)
        k4 = h * hilfsfunktion(startvalues + k3, t + h, current)
        startvalues += (k1 + 2 * k2 + 2 * k3 + k4) / 6
    return U


def scipymethode(startvalues, N, current):

    """ Hier wird die Odeint Methode aus dem Scipy Paket verwendet.
        Der Solver hat dabei die gleich definierten Parameter, timesteps und h wie die Funktionen vorher.
        Auch hier erhalten wir wieder einen Spannungsverlauf."""

    h = (t_ende - t_start) / N
    timesteps = np.arange(t_start, t_ende, h)
    solved = odeint(hilfsfunktion, startvalues, timesteps, args=(current,))
    U = solved.T[0]
    return U


# Funktionen für die Aufgabe 2d

def impuls(t, I_imp):

    """Diese Funktion soll einen Stromimpuls zum Zeitpunkt t = 10ms für 1ms ausgeben für die Impulsaufgabe."""

    if 10 <= t <= 11:
        return I_imp
    else:
        return 0


def hilfsfunktion_ein_impuls(startvalues, t, current, I_imp):

    """ Die Idee der Funktion ist die selber wie in der Hilfsfunktion davor.
        Der neue Paramter I_imp ist dabei die Stromstärke, die der Impuls haben soll.
        Hierbei wird der Paramter t der Funktion impuls übergeben, damit wir im gewünschten Zeitraum den Stromimpuls erhalten.
        Die Spannungs DGL hf1 wird dabei um den Strom des Impulses erweitert"""

    U_given = startvalues[0]
    n = startvalues[1]
    m = startvalues[2]
    h = startvalues[3]
    hf1 = (current - I_K(U_given, n) - I_Na(U_given, m, h) - I_L(U_given) + impuls(t, I_imp)) / C
    hf2 = alpha_n(U_given) * (1 - n) - beta_n(U_given) * n
    hf3 = alpha_m(U_given) * (1 - m) - beta_m(U_given) * m
    hf4 = alpha_h(U_given) * (1 - h) - beta_h(U_given) * h
    return np.array([hf1, hf2, hf3, hf4], float)


def runge_kutta_ein_impuls(N, startvalues, current, I_imp):

    """ Auch hier ist die Idee wieder dieselbe wie in der Runge-Kutta Methode davor.
        In dieser hat sich auch nichts verändert außer, dass hier die Impulsstärke übergeben wird.
        Die Gatingvariablen werden hier nun ausgegeben, um in Aufgabe 2d) visualisiert werden zu können."""

    h = (t_ende - t_start) / N
    timesteps = np.arange(t_start, t_ende, h)
    U = []
    n_list = []
    m_list = []
    h_list = []
    for t in timesteps:
        U.append(startvalues[0])
        n_list.append(startvalues[1])
        m_list.append(startvalues[2])
        h_list.append(startvalues[3])
        k1 = h * hilfsfunktion_ein_impuls(startvalues, t, current, I_imp)
        k2 = h * hilfsfunktion_ein_impuls(startvalues + 0.5 * k1, t + 0.5 * h, current, I_imp)
        k3 = h * hilfsfunktion_ein_impuls(startvalues + 0.5 * k2, t + 0.5 * h, current, I_imp)
        k4 = h * hilfsfunktion_ein_impuls(startvalues + k3, t + h, current, I_imp)
        startvalues += (k1 + 2 * k2 + 2 * k3 + k4) / 6
    return U, n_list, m_list, h_list


# Funktionen für Aufgabe 3 a-c)

def impulse(t, I_imp, t1, t2):

    """Hier werden zwei Impulse, gleicher Stärke, ausgegeben. Dabei soll t2 immer größer als t1 sein."""

    if t2 > t1:
        if t1 <= t <= t1 + 1:  # in der Aufgabe ist t1 := 10ms immer
            return I_imp
        if t2 <= t <= t2 + 1:
            return I_imp
        else:
            return 0
    else:
        return 0


def hilfsfunktion_impulse(startvalues, t, current, t1, t2, I_imp):

    """ Diese Hilfsfunktion greift auf die Funktion der mehreren Impulse zu.
        Deshalb übergeben wir hier auch die Zeitpunkte t1 und t2, an denen diese stattfinden sollen."""

    U_given = startvalues[0]
    n = startvalues[1]
    m = startvalues[2]
    h = startvalues[3]
    hf1 = (current - I_K(U_given, n) - I_Na(U_given, m, h) - I_L(U_given) + impulse(t, I_imp, t1, t2)) / C
    hf2 = alpha_n(U_given) * (1 - n) - beta_n(U_given) * n
    hf3 = alpha_m(U_given) * (1 - m) - beta_m(U_given) * m
    hf4 = alpha_h(U_given) * (1 - h) - beta_h(U_given) * h
    return np.array([hf1, hf2, hf3, hf4], float)




def runge_kutta_impulse(N, startvalues, current, t1, t2, I_imp):

    """ Auch hier ist alles dasselbe wie vorher und hier werden nun die Zeitpunkte t1 und t2 übergeben, an denen es stattfinden soll.
        Die returns sind hierbei auch noch die Gatingvariablen, denn diese sollen in der Aufgabe 3 dargestellt werden."""

    h = (t_ende - t_start) / N
    timesteps = np.arange(t_start, t_ende, h)
    U = []
    n_list = []
    m_list = []
    h_list = []
    for t in timesteps:
        U.append(startvalues[0])
        n_list.append(startvalues[1])
        m_list.append(startvalues[2])
        h_list.append(startvalues[3])
        k1 = h * hilfsfunktion_impulse(startvalues, t, current, t1, t2, I_imp)
        k2 = h * hilfsfunktion_impulse(startvalues + 0.5 * k1, t + 0.5 * h, current, t1, t2, I_imp)
        k3 = h * hilfsfunktion_impulse(startvalues + 0.5 * k2, t + 0.5 * h, current, t1, t2, I_imp)
        k4 = h * hilfsfunktion_impulse(startvalues + k3, t + h, current, t1, t2, I_imp)
        startvalues += (k1 + 2 * k2 + 2 * k3 + k4) / 6
    return U, n_list, m_list, h_list
