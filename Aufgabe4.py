import matplotlib.pyplot as plt
import numpy as np
from matplotlib import gridspec
from neuronen_verlagerung.neuronen_rechnung import runge_kutta_ein_impuls

"""Das hier sind die Trainingsschachbretter und der jeweilige erwartete Wert des fünften Neurons."""

input_training = np.array(
    [[1, 0, 1, 0], [0, 0, 1, 1], [0, 1, 0, 1], [1, 0, 0, 1], [1, 0, 0, 1], [1, 0, 0, 1], [0, 1, 1, 0]], float)

training_output = np.array([-65, -65, -65, 45.88885778168372, 45.88885778168372, 45.88885778168372, -65, -65]).T


def sigmoidfunktion(x):
    """ Diese Funktion verläuft zwischen [0,1]. Deshalb verwenden wir sie um unsere Gewichte später anzupassen."""

    return 1 / (1 + np.exp(-x))


def handleinput(int):
    """ Hier soll die maximale Spannunge# des Aktionspotentiales oder das Ruhepotential ausgegeben werden, jenachdem welches Feld
        des Schachrettes betrachet wird, also diehnt es zur Übersetzung."""

    if int == 1:
        output = np.max(runge_kutta_ein_impuls(1000, np.array([-65, 0.5, 0.01, 0.99]), -5, 20)[0])
        return output
    else:
        return -65


def getOutput(chessboard):
    """ Das hier ist die Aufgabe 4c).
        Dabei wird das als Parameter übergebene Schachbrett in Spannungen übersetzt.
        Danach wird es als Skalarprodukt mit den Gewichten verrechnet.
        Wenn der Strom mindestens 20 nA ist, soll es als Schachbrett erkannt werden."""

    weights1 = [1, 1, 1, 1]
    weights2 = [1, 0, 0, 1]
    for i in range(len(chessboard)):
        chessboard[i] = handleinput(chessboard[i])
    print(chessboard)
    current_value = np.dot(weights2, chessboard)
    print(current_value)
    if current_value >= 20:
        return "This is a Chessboard"
    return "This ISNT a Chessboard"


def learning(training_chessboards, iterations, expected_output):
    """ Die Lernmethode hat die Parameter:
        training_chessboards, diese sind standardmäßig immer die input_training boards
        iterations, gibt die Zahl wie häufig der Lernzyklus durchlaufen wird,
        expected_output, sind standardmäßig die training_output Werte.
        Wir speichern die Anfangsgewichte ab und kopieren sie in die calc_weights, damit wir sie am ende vergleichen können.
        Die weights_list_i stehen für die verschiedenen Positionen und werden am ende übergeben, damit wir die Entwicklung
        jeder Position als Plot nachvollziehen können.
        Ansonsten werden pro Iteration alle Schachbretter aus dem training_chessboards einmal durchgegangen, um die Gewichte an den Stellen anzupassen, an denen eine 1 ist.
        Wir multiplizieren das Ergebnis mit den Komponenten der Gewichte damit wir immer echt größer als 0 sind.
        Dabei begrezen wir jedoch unsere maximalen Gewichte, da diese aus (0,1] sein sollen.
        Als return geben wir die Arrays der jeweiligen Positionen aus, damit wir diese in der nächsten Funktion ploten können.
        """

    initial_weights = [np.random.random(), np.random.random(), np.random.random(), np.random.random()]
    calc_weights = np.copy(initial_weights)
    board_in_current = np.copy(training_chessboards)
    weights_list_1 = []
    weights_list_2 = []
    weights_list_3 = []
    weights_list_4 = []

    for j in range(iterations):
        for i in range(len(training_chessboards)):
            weights_list_1.append(calc_weights[0])
            weights_list_2.append(calc_weights[1])
            weights_list_3.append(calc_weights[2])
            weights_list_4.append(calc_weights[3])
            for z in range(4):
                if board_in_current[i][z] == 1:
                    board_in_current[i][z] = handleinput(1)
                else:
                    board_in_current[i][z] = handleinput(0)
                board_expected_output = expected_output[i]
                current_impuls = np.dot(calc_weights, board_in_current[i])
                if current_impuls <= -5:
                    current_impuls = -5
                real_output = np.max(
                    runge_kutta_ein_impuls(1000, np.array([-65, 0.5, 0.01, 0.99]), -5, current_impuls)[0])
                for k in range(4):
                    if training_chessboards[i][k] == 1:
                        calc_weights[k] = calc_weights[k] * (training_chessboards[i][k] + sigmoidfunktion(
                            (board_expected_output - real_output)) - 0.55)
                        if calc_weights[k] > 1.0:
                            calc_weights[k] = 1.0
    weights_list_1.append(calc_weights[0])
    weights_list_2.append(calc_weights[1])
    weights_list_3.append(calc_weights[2])
    weights_list_4.append(calc_weights[3])
    print(initial_weights)
    print(calc_weights)
    return weights_list_1, weights_list_2, weights_list_3, weights_list_4


def plot_weights():
    """ Hier wird die Entwicklung unserer Lernfunktion geplotet.
        Dabei speichern wir die returns von learning in Arrays.
        Diese Arrays werden dann später geplotet und beschreiben die jeweilige Entwicklung der positionen der Gewichte.
        Die right und false counter sind Zähler für die Anzahl der richtigen und falschen Ergebnisse.
        Diese werden dann geplotet, um zu zeigen, wie gut das Programm lernt.
        Die current_i Werte sind die berechneten Impulse für die Bretter [1,0,0,1], [1,0,1,0], [0,1,0,1], [0,1,1,0], [1,1,0,0] und [0, 0, 1, 1].
        Dabei wird jedes mal ein Array erstellt, dass den Gewichten aus der Iteration der learning Methode entspricht.
        Wenn die Impulse verrechnet mit der Runge-Kutta Methode die jeweiligen erwarteten Werte, also für [1,0,0,1] eine Spannung größer 0 liefert
        und alle anderen eine Spannung kleiner als 0 haben, dann soll es als richtig erkannt werden.
        Ansonsten ist es falsch und wird somit als falsch auch berechnet.
        Am ende ploten wir dann die Gewichtungen über Iterationen und die Richtigkeit des Lernprozesses über die Iterationen"""

    iterationen = 15
    weights_1, weights_2, weights_3, weights_4 = learning(input_training, iterationen, training_output)
    lenght = len(weights_1)
    right = np.zeros(lenght)
    false = np.zeros(lenght)
    right_counter = 0
    false_counter = 0
    weights = np.zeros(4)
    gs = gridspec.GridSpec(1, 2, width_ratios=[1.5, 1.5])
    plt.subplot(gs[0])
    for j in range(lenght):
        weights[0] = weights_1[j]
        weights[1] = weights_2[j]
        weights[2] = weights_3[j]
        weights[3] = weights_4[j]
        current1 = np.dot(weights, [45.23709970444943, -65, -65, 45.23709970444943])
        current2 = np.dot(weights, [45.23709970444943, -65, 45.23709970444943, -65])
        current3 = np.dot(weights, [-65, 45.23709970444943, -65, 45.23709970444943])
        current4 = np.dot(weights, [-65, 45.23709970444943, 45.23709970444943, -65])
        current5 = np.dot(weights, [45.23709970444943, 45.23709970444943, -65, -65])
        current6 = np.dot(weights, [-65, -65, 45.23709970444943, 45.23709970444943])
        if current1 < -5:
            current1 = -5
        if current2 < -5:
            current2 = -5
        if current3 < -5:
            current3 = -5
        if current4 < -5:
            current4 = -5
        if current4 < -5:
            current4 = -5
        if current5 < -5:
            current5 = -5
        if current6 < -5:
            current6 = -5
        voltage1 = np.max(runge_kutta_ein_impuls(1000, np.array([-65, 0.5, 0.01, 0.99]), -5, current1)[0])
        voltage2 = np.max(runge_kutta_ein_impuls(1000, np.array([-65, 0.5, 0.01, 0.99]), -5, current2)[0])
        voltage3 = np.max(runge_kutta_ein_impuls(1000, np.array([-65, 0.5, 0.01, 0.99]), -5, current3)[0])
        voltage4 = np.max(runge_kutta_ein_impuls(1000, np.array([-65, 0.5, 0.01, 0.99]), -5, current4)[0])
        voltage5 = np.max(runge_kutta_ein_impuls(1000, np.array([-65, 0.5, 0.01, 0.99]), -5, current5)[0])
        voltage6 = np.max(runge_kutta_ein_impuls(1000, np.array([-65, 0.5, 0.01, 0.99]), -5, current6)[0])
        if voltage1 > 0 and voltage2 < 0 and voltage3 < 0 and voltage4 < 0 and voltage5 < 0 and voltage6 < 0:
            right_counter += 1
            right[j] = right_counter
            false[j] = false_counter
        else:
            false_counter += 1
            false[j] = false_counter
            right[j] = right_counter
    plt.plot(range(lenght), weights_1, label="Weights 1")
    plt.plot(range(lenght), weights_2, label="Weights 2")
    plt.plot(range(lenght), weights_3, label="Weights 3")
    plt.plot(range(lenght), weights_4, label="Weights 4")
    plt.ylabel(r"Gewichtungen /epsilon (0,1]")
    plt.xlabel("Iterationen")
    plt.legend(loc=0)
    plt.subplot(gs[1])
    plt.plot(range(lenght), right, label="Richtig")
    plt.plot(range(lenght), false, label="Falsch")
    plt.ylabel("Richtig/Falsch Anzahl")
    plt.xlabel("Iterationen")
    plt.legend(loc=1)
    plt.show()


plot_weights()
