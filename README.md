# Simulation of Neural Network - Hodgkin and Huxley

This is a project for Computational Physics at TU Darmstadt created in 2020.

More information can be found in
[Nico's blog post](https://nico.dorfbrunnen.eu/de/posts/2020/neuronal/)
and in [the report (German)](report.pdf).

## Running

Development happens with Pycharm, so it's best to just clone the repository and import it there.

To run this program, you need to install the Python dependencies:
```bash
pip3 install -r requirements.txt
```

You can then start the program by e.g. calling `python3 Aufgabe2.py`.

## License

Everything is licensed under MIT. See [LICENSE.md](LICENSE.md) for more information.
