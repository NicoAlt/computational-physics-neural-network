import numpy as np
import matplotlib.pyplot as plt
from matplotlib import gridspec
from neuronen_verlagerung.neuronen_rechnung import runge_kutta, scipymethode, expliciteuler, runge_kutta_ein_impuls

"""Zeit Intervall"""

t_ende = 50
t_start = 0

"""Ruheotential"""

V_pot = -65


# Aufgabe 2a in neuronen_verlagerung


def plot_all_functions_comparison(current):  # Aufgabe 2b)

    """ In dieser Funktion greifen wir auf die im neuronen_rechner Package implementierten Funktionen zurück.
        Der Paramter current steht für den konstanten Strom.
        Das N_points Array soll dabei ein Array mit verschiedenen Stützstellenanzahlen sein.
        In jeder Iteration der Schleife wird dabei der Abstand der Stützstellen neu berechnet
        sowie der Spannungsverlauf mit der Runge-Kutta, Odeint und expliziten Euler Methode.
        Danach werden die Graphen geplotet."""

    N_points = [200, 300, 500, 1000, 2500]
    index = 1
    for N in N_points:
        plt.subplot(3, 2, index)
        h = (t_ende - t_start) / N
        timesteps = np.arange(t_start, t_ende, h)
        plt.plot(timesteps, runge_kutta(N, np.array([V_pot, 0.5, 0.01, 0.99], float), current),
                 label=f'U Runge-Kutta,N: {N_points[index-1]}', color="red", linestyle="dotted")
        plt.plot(timesteps, expliciteuler(N, np.array([V_pot, 0.5, 0.01, 0.99], float), current),
                 label=f'U Euler,N: {N_points[index-1]}', linestyle="dashed")
        plt.plot(timesteps, scipymethode(np.array([V_pot, 0.5, 0.01, 0.99], float), N, current),
                  label=f"U scipy,N: {N_points[index-1]}", color="green", linestyle=(0, (5, 10)))
        plt.legend()
        plt.xlabel("Time [ms]")
        plt.ylabel("Voltage [mV]")
        index += 1
    plt.show()

#plot_all_functions_comparison(-5)


# Aufgabe 2c) mit Stromstärken -5, -2, ... , 15 nA


def plot_diffrent_currents(N, startvalues):

    """ Die Parameter stehen für die Stützstellenanzahl und die Anfangsbedingungen.
        Hier wird immer wieder der konstante Strom angepasst, dabei befinden wir uns zwischen -5nA und 15nA.
        Wir gehen in 4er Schritten nach oben nach jeder Iteration.
        Wir greifen hier auf die Runge-Kutta Methode zurück, da sie genauer ist als die Euler Methode.
        """

    h = (t_ende - t_start) / N
    timesteps = np.arange(t_start, t_ende, h)
    current = -5
    for index in range(6):
        index += 1
        plt.subplot(3, 3, index)
        plt.plot(timesteps, runge_kutta(N, startvalues, current), label=f"U Runge_Kutta, I: {current}nA ")
        current += 4
        plt.legend(loc=1)
        plt.xlabel("Time [ms]")
        plt.ylabel("Voltage [mV]")
    plt.show()

#plot_diffrent_currents(10000, np.array([V_pot, 0.5, 0.01, 0.99]))

# Aufgabe 2d) mit Impuls 50 nA bei 10s < t < 11s


def impuls_visualisierung(N, startvalues):

    """ Diese Funktion ist im Prinzip auch wieder dasselbe.
        Wir kopieren unser Zeitarray in ein current Array.
        Das machen wir, damit wir über die Boolean Beziehung entscheiden können, bei welchem Index der Stromimpuls visualisiert wird.
        Die Beziehung gibt ein True an jeder Stelle aus an der 10ms <= t <= 11ms, ansonsten soll überall der konstante Strom sein.
        Hier wird nun auf die Runge-Kutta Methode mit dem Impuls zugegriffen.
        Dabei bekommen wir alle Werte für die Spannung und die Gatingvariablen.
        Diese werden dann geplotet."""

    h = (t_ende - t_start) / N
    timesteps = np.arange(t_start, t_ende, h)
    current = np.copy(timesteps)
    current[current <= 10] = -5
    current[current >= 11] = -5
    current[current != -5] = 50
    U_points, n_points, m_points, h_points = runge_kutta_ein_impuls(N, startvalues, -5, 50)
    gs = gridspec.GridSpec(1, 2, width_ratios=[2, 2])  # Ein "Gitter" das die größen der Subplots beschreibt
    plt.subplot(gs[0])
    plt.plot(timesteps, U_points, label=r"U Runge_Kutta")
    plt.plot(timesteps, current, label="Electric current")
    plt.xlabel("Time [ms]")
    plt.ylabel("Voltage [mV] / Electric Current [nA]")
    plt.legend()
    plt.subplot(gs[1])
    plt.plot(timesteps, n_points, label="n")
    plt.plot(timesteps, m_points, label="m")
    plt.plot(timesteps, h_points, label="h")
    plt.xlabel("Time [ms]")
    plt.ylabel("Gatingvariables")
    plt.legend()
    plt.show()


impuls_visualisierung(10000, np.array([V_pot, 0.5, 0.01, 0.99]))
